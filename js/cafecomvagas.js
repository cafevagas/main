$(document).ready(function () {

    if ($('.descomplique-busca').length > 0) {
        $('.name-logged').css('color', '#FFF');
    }

    /*INICIO selectize*/
    if ($('select').length > 0) {
        $('select').each(function () {
            if ($(this).hasClass('select-add')) {
                $(this).selectize({
                    create: function (input) {
                        return {value: slug(input), text: input};
                    }
                });
            } else {
                $(this).selectize();
            }
        });
    }

    /*FIM selectize*/

    /*INICIO CAROUSEL VAGAS HOME*/
    if ($('.owl-carousel').length) {
        $('.owl-carousel').owlCarousel({
            nav: true,
            navText: ["<img src='img/prev.png' class='owl-prev'>", "<img src='img/next.png' class='owl-next'>"],
            loop: false,
            margin: 58,
            responsiveClass:    true,
            responsive: {
                0: {
                    items: 1,
                },
                600: {
                    items: 2,
                },
                1000: {
                    items: 3,
                }
            }
        })
    }
    /*FIM CAROUSEL VAGAS HOME*/

    /*INICIO INPUT MATERIAL DESIGN EFFECT*/
    validacao();
    /*FIM INPUT MATERIAL DESIGN EFFECT*/

    /*INICIO VALIDAR CAMPO DE SENHA*/
    $("form[name='cadastro']").submit(function (event) {
        var senha = $('#senha');
        var repetirsenha = $('#repetirSenha');
        if (repetirsenha.val() !== senha.val()) {
            repetirsenha.focus();
            alert('Repita a senha corretamente');
            return false;
        }
    });
    /*FIM VALIDAR CAMPO DE SENHA*/

    /*INICIO MENU MOBILE*/

//    LINK HOME
    if ($('.mobile-link-home').length) {
        $('.mobile-link-home').click(function () {
            window.location.href = "http://aws.vinteum.com/pluguin/cafecomvagas/";
        });
    }
    /*FIM MENU MOBILE*/

//    VALIDA EXTENSÃO FOTO USUARIO E FOTO EMPRESA
    $('input[name=foto-empresa]').change(function () {
        var file = $(this).val();
//        console.log(file.height());
//        console.log(file.width());
        switch (file.substring(file.lastIndexOf('.') + 1).toLowerCase()) {
            case 'gif':
            case 'jpg':
            case 'jpge':
            case 'png':
//                alert("OK");
//                break;
            default:
                $(this).val('');
                // error message here
                alert("Não é uma imagem");
                break;
        }
    });

//  INICIO PAINEL TOGGLE
    if ($('.vagas-candidatos').length > 0) {
        $('.panel-default').on('show.bs.collapse', function () {
            var experiencia = $(this).find('.experiencias');
            var graduacao = $(this).find('.graduacoes');
            var publicacao = $(this).find('.publicacoes');

            $(this).find('.candidato-header').toggle();
            $(this).find('.candidato-header-open').fadeToggle("slow", "linear");

            if (experiencia.length > 2) {
                verTodas(experiencia);
            }
            if (graduacao.length > 2) {
                verTodas(graduacao);
            }
            if (publicacao.length > 2) {
                verTodas(publicacao);
            }
        });
        $('.panel-default').on('hide.bs.collapse', function () {
            $(this).find('.candidato-header').fadeToggle("slow", "linear");
            $(this).find('.candidato-header-open').toggle();
        });
        $('.ver-todas').click(function () {
            $(this).siblings(':gt(2)').fadeToggle("slow", "linear");
            $(this).css('display', 'none');
        });
    }
//    FIM PAINEL TOGGLE

////////////////////////////////////////////////
//    DELETAR
//      EXIBIR ALERTA
    if ($('.alert-custom').length) {
        alertInfo()
    }
//    DELETAR
////////////////////////////////////////////////

//INICIO MODAL CADASTRO USUARIO
    $('.nao-cadastrado').click(function () {
        $('.login-modal').modal('toggle');
        $('.dados-iniciais-modal').modal('toggle');
    });
//FIM MODAL CADASTRO USUARIO

//INICIO MODAL DADOS INICIAIS
    $('.dados-iniciais-btn').click(function () {
        $('.dados-iniciais-modal').modal('toggle');
        $('.codigo-confirmacao-modal').modal('toggle');
    });
//FIM MODAL DADOS INICIAIS

//INICIO MODAL CÓDIGO DE CONFIRMAÇÃO
    $('.codigo-confirmacao-verificar').click(function () {
        $('form[name=codigo-confirmacao]').toggle();
        $('.codigo-confirmacao-sucesso').fadeToggle("slow", "linear");
    });
//FIM MODAL CÓDIGO DE CONFIRMAÇÃO

//INICIO DATA TOGGLE
    if ($('[data-toggle="popover"]').length > 0) {
        $('[data-toggle="popover"]').popover();
    }
//FIM DATA TOGGLE

//INICIO REDEFINIR SENHA
    $('form .login-linkedin').submit(function () {

    });
//FIM REDEFINIR SENHA

//INICIO VAGAS
    if ($('.vagas').length > 0) {
        if (!(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))) {
            $('.panel-default').on('show.bs.collapse', function () {
                $(this).find('.logo-empresa').fadeToggle("fast", "linear");
            });
            $('.panel-default').on('hide.bs.collapse', function () {
                $(this).find('.logo-empresa').fadeToggle("fast", "linear");
            });
        }
    }
//FIM VAGAS
});
$(window).load(function () {
    setTimeout(function () {
        $('form :input:visible').each(function (index, element) {
            var id = $(this).attr('id');
            var length = $('form :input:visible').length;
            if (id) {
                var name = id.split('-');
                if (name.pop() !== 'selectized') {
                    $(this).focus();
                }
            }
            if (index === (length - 1)) {
                $('form :input:visible')[0].focus();
            }
        });
    }, 250)
})

function validacao() {
    /*INICIO INPUT MATERIAL DESIGN EFFECT*/
    $('input, textarea, select, .selectize-input input').focusin(function () {
        var input = $(this).attr('id');
        if ($(this).attr('type') !== 'checkbox' && $(this).attr('type') !== 'radio' && $(this).attr('type') !== 'file') {

            if (input.split("-").pop() === 'selectized') {
                var parentEls = $(this).parentsUntil('.form-group');
                var id = input.replace('-selectized', '');

                $('#' + id).removeClass('check-empty check-true check-false');
                $(parentEls[1]).removeClass('check-empty check-true check-false');
                $(parentEls[0]).removeClass('check-empty check-true check-false');
            } else {
                $(this).removeClass('check-empty check-true check-false');
            }

        }
    });
    $('input, textarea, select, .selectize-input input').focusout(function () {
        var input = $(this).attr('id');

        if ($(this).attr('type') !== 'checkbox' && $(this).attr('type') !== 'radio' && $(this).attr('type') !== 'file') {

            if (input.split("-").pop() === 'selectized') {
                var parentEls = $(this).parentsUntil('.form-group');
                var id = input.replace('-selectized', '');

                setTimeout(function () {

                    if ($('#' + id).val().length > 0) {
                        if ($('#' + id).get(0).checkValidity()) {

                            $('#' + id).addClass('check-true');
                            $(parentEls[0]).addClass('check-true');
                        } else {
                            $('#' + id).addClass('check-false');
                            $(parentEls[0]).addClass('check-false');
                        }
                    } else {
                        $('#' + id).addClass('check-empty');
                        $(parentEls[0]).addClass('check-empty');

                    }

                }, 50);
            } else {

                if ($(this).val()) {

                    if ($(this).get(0).checkValidity()) {

                        $(this).addClass('check-true');
                    } else {
                        $(this).addClass('check-false');
                    }
                } else {
                    $(this).addClass('check-empty');
                }
            }
        }
    });
    /*INICIO INPUT PLACEHOLDER*/
    //    TELEFONE e TELEFONE FIXO e TELEFONE CELULAR
    $('input[name=telefone], input[name=telefone-celular]').focusin(function () {
        $(this).attr("placeholder", "(xx) xxxxx-xxxx");
    });
    $('input[name=telefone-fixo]').focusin(function () {
        $(this).attr("placeholder", "(xx) xxxx-xxxx");
    });
    $('input[name=telefone], input[name=telefone-fixo], input[name=telefone-celular]').focusout(function () {
        if ($(this).val().trim() === '') {
            $(this).attr("placeholder", "");
        }
    });
    //    CPF
    $('input[name=cpf]').focusin(function () {
        $(this).attr("placeholder", "xxx.xxx.xxx-xx");
    });
    $('input[name=cpf]').focusout(function () {
        if ($(this).val().trim() === '') {
            $(this).attr("placeholder", "");
        }
    });
    //    CEP
    $('input[name=cep]').focusin(function () {
        $(this).attr("placeholder", "xxxxx-xxx");
    });
    $('input[name=cep]').focusout(function () {
        if ($(this).val().trim() === '') {
            $(this).attr("placeholder", "");
        }
    });
    //    CNPJ
    $('input[name=cnpj]').focusin(function () {
        $(this).attr("placeholder", "xx.xxx.xxx/xxxx-xx");
    });
    $('input[name=cnpj]').focusout(function () {
        if ($(this).val().trim() === '') {
            $(this).attr("placeholder", "");
        }
    });
    //    DATA
    $('.data-mask').focusin(function () {
        $(this).attr("placeholder", "xx/xx/xxxx");
    });
    $('.data-mask').focusout(function () {
        if ($(this).val().trim() === '') {
            $(this).attr("placeholder", "");
        }
    });
    //    HABILIDADES DESTAQUE
    $('input[name=primeiro-lugar]').focusin(function () {
        $(this).attr("placeholder", "Exemplo: Montar relatórios");
    });
    $('input[name=primeiro-lugar]').focusout(function () {
        if ($(this).val().trim() === '') {
            $(this).attr("placeholder", "");
        }
    });
    /*FIM INPUT PLACEHOLDER*/

//    MASK
//    TELEFONE
    if ($('input[name=telefone]').length) {
        $('input[name=telefone]').mask('(00) 0000-0000#', {
            onComplete: function (tel) {
                $('input[name=telefone]').mask('(00) 00000-0000');
            }});
    }
//    TELEFONE FIXO
    if ($('input[name=telefone-fixo]').length) {
        $('input[name=telefone-fixo]').mask('(00) 0000-0000');
    }
//    TELEFONE CELULAR
    if ($('input[name=telefone-celular]').length) {
        $('input[name=telefone-celular]').mask('(00) 00000-0000');
    }
//    IDADE
    if ($('.idade-mask').length) {
        $('.idade-mask').mask("00");
    }
//    CEP
    if ($('input[name=cep]').length) {
        $('input[name=cep]').mask("00000-000");
    }
//    CPF
    if ($('input[name=cpf]').length) {
        $('input[name=cpf]').mask("000.000.000-00");
    }
//    CNPJ
    if ($('input[name=cnpj]').length) {
        $('input[name=cnpj]').mask("00.000.000/0000-00");
    }
//    MOEDA (R$)
    if ($('.moeda-mask').length) {
        $('.moeda-mask').mask('000.000.000.000.000,00', {reverse: true});
    }
//    DATA
    if ($('.data-mask').length) {
        $('.data-mask').mask('00/00/0000');
    }
//    FIM MASK
}

function redefinirSenhaSucesso() {
    $("form[name=login-linkedin]").toggle();
    $(".redefinir-senha-sucesso").fadeToggle();
    setTimeout(function () {
        window.location.href = "http://aws.vinteum.com/pluguin/cafecomvagas/";
    }, 3000);
}
function alertInfo() {
    //Mensagem de alerta
    $(".alert-custom").fadeToggle();
    setTimeout(function () {
        $(".alert-custom").fadeToggle();
    }, 3000);
}
function verTodas(e) {
    e.siblings('.ver-todas').css('display', 'block');
    e.hide().filter(':lt(2)').show();
}

function menuMobile(e) {
    var id = $(e).attr('id');
    var name = id.split('-').pop();
    if ($(e).hasClass('menu-active')) {
        $('.menu-mobile-' + name).addClass('menu-mobile-hidden');
        $('.menu-mobile-' + name).removeClass('menu-mobile-show');
        $(e).removeClass('menu-active');
    } else {

        if ($('.menu-mobile-show')) {
            menuMobileFechar();
        }
        if ($('.menu-mobile-' + name).hasClass('menu-mobile-hidden')) {
            $('.menu-mobile-' + name).addClass('menu-mobile-show');
            $('.menu-mobile-' + name).removeClass('menu-mobile-hidden');
            $(e).addClass('menu-active');
        }
    }
}
function menuMobileFechar() {
    $('.mobile-itens').each(function () {

        if ($(this).children('div').hasClass('menu-mobile-show')) {
            $('.menu-mobile-show').addClass('menu-mobile-hidden');
            $('.menu-mobile-show').removeClass('menu-mobile-show');
            $('.menu-active').removeClass('menu-active');
        }
    });
}

function adicionarCard(select) {
    var ul = $('.' + $(select).attr('id') + '-cards');
    var avaliacao = '';
    if (ul.find('input[value=' + slug($(select).val()) + ']').length == 0) {

// <li class="row">
// <div class="col-md-12">Frontend <i class="fa fa-times-circle remover-card" aria-hidden="true" onclick="removerCard(this)"></i></div>
// <div class="col-md-12 hr"><hr></div>
// <div class="col-md-12">Nível desejado 2/10 
// <i class="fa fa-star" aria-hidden="true"></i>
//  <i class="fa fa-star-o" aria-hidden="true"></i>
//    <i class="fa fa-star-o" aria-hidden="true"></i>
//      <i class="fa fa-star-o" aria-hidden="true"></i>
//        <i class="fa fa-star-o" aria-hidden="true"></i>
// </div>
// </li>

//        if ($(select).attr('id') == 'habilidades' || $(select).attr('id') == 'idiomas') {
//            avaliacao = '<div class="col-md-5 col-sm-5 text-right">Nivel iniciante 3/10</div>' +
//                    '<div class="col-md-3 col-md-3 text-center">' +
//                    '<i class="fa fa-star-o" aria-hidden="true"></i> ' +
//                    '<i class="fa fa-star-o" aria-hidden="true"></i> ' +
//                    '<i class="fa fa-star-o" aria-hidden="true"></i> ' +
//                    '<i class="fa fa-star-o" aria-hidden="true"></i> ' +
//                    '<i class="fa fa-star-o" aria-hidden="true"></i> ' +
//                    '</div>';
//        }

        ul.append('<li class="row">' +
                '<input type="hidden" name="' + $(select).attr('id') + '" value="' + slug($(select).val()) + '" /> ' +
                '<div class="col-md-12">' + $(select).find('option:selected').text() + ' <i class="fa fa-times-circle remover-card" aria-hidden="true" onclick="removerCard(this)"></i></div>' +
                '<div class="col-md-12 hr"><hr></div>' +
                '<div class="col-md-12">Nível desejado 2/10 ' +
                '<div class="pull-right"> ' +
                '<i class="fa fa-star" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o" aria-hidden="true"></i> ' +
                '<i class="fa fa-star-o" aria-hidden="true"></i>' +
                '</div>' +
                '</div>' +
                '</li>');
    }
}
function adicionarTags(select) {
    if ($(select).val().length > 0) {
        var ul = $('.' + $(select).attr('id') + '-tags');
        if (ul.find('input[value=' + slug($(select).val()) + ']').length == 0) {
            ul.append('<li>' +
                    '<input type="hidden" name="' + $(select).attr('id') + '" value="' + slug($(select).val()) + '" /> ' +
                    $(select).find('option:selected').text() + ' ' +
                    '<i class="fa fa-times-circle remover-card" aria-hidden="true" onclick="removerTag(this)"></i>' +
                    '</li>');
        }
    }
}
function adicionarExperiencia() {

    if (!$('.card-inputs').closest('form').get(0).checkValidity()) {
        $("#" + document.querySelectorAll(":invalid")[1].id).focus();
        alertInfo();
        return false;
    }

    var exp = $('.card-input-titulo');
    var countExp = exp.length;
    var idExp;

    if (countExp > 0) {
        idExp = parseInt(exp.last().attr('id').split('-').pop()) + 1;
    } else {
        idExp = 1;
    }

    $('.card-inputs').append('<div class="col-md-6 col-md-offset-3 col-xs-12 bg-primary-sweet card-' + idExp + '">' +
            '<div class="experiencia-profissional-' + idExp + '">' +
            '<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 card-input-titulo" id="exp-titulo-' + idExp + '">' +
            '<h5>Experiência Profissional ' + idExp + '</h5>' +
            '<i class="fa fa-times-circle remover_card" onclick="removerCardInputs(this)" aria-hidden="true" style="cursor: pointer;"></i>' +
            '</div>' +
            '<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">' +
            '<div class="form-group">' +
            '<div class="col-md-12">' +
            '<input type="text" class="form-control check-empty" name="nome-empresa[]" id="experiencia-nome-empresa-' + idExp + '" pattern=".{2,}" title="Nome da empresa (opcional)">' +
            '<label for="experiencia-nome-empresa-' + idExp + '">Nome da empresa (opcional)</label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-12">' +
            '<div class="form-group">' +
            '<div class="col-md-12">' +
            '<input type="tel" class="form-control data-mask check-empty" name="data-inicio[]" id="experiencia-data-inicio-' + idExp + '" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" title="Data de início" required>' +
            '<label for="experiencia-data-inicio-' + idExp + '">Data de início</label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 col-sm-4 col-xs-12">' +
            '<div class="form-group">' +
            '<div class="col-md-12">' +
            '<input type="tel" class="form-control data-mask check-empty" name="data-fim[]" id="experiencia-data-fim-' + idExp + '" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" title="Data de término" required>' +
            '<label for="experiencia-data-fim-' + idExp + '">Data de término</label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">' +
            '<div class="checkbox">' +
            '<input type="checkbox" class="form-control" name="cargo-atual[]" id="experiencia-cargo-atual-' + idExp + '" title="Cargo atual" value="true">' +
            '<label for="experiencia-cargo-atual-' + idExp + '">Cargo atual</label>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">' +
            '<div class="form-group">' +
            '<div class="col-md-12">' +
            '<input type="text" class="form-control check-empty" name="segmento[]" id="experiencia-segmento-' + idExp + '" pattern=".{2,}" title="Segmento" required>' +
            '<label for="experiencia-segmento-' + idExp + '">Segmento</label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">' +
            '<div class="form-group">' +
            '<div class="col-md-12">' +
            '<input type="text" class="form-control check-empty" name="cargo[]" id="experiencia-cargo-' + idExp + '" pattern=".{2,}" title="Cargo" required>' +
            '<label for="experiencia-cargo-' + idExp + '">Cargo</label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">' +
            '<div class="form-group">' +
            '<div class="col-md-12">' +
            '<textarea class="form-control check-empty" name="resumo[]" id="experiencia-resumo-1" title="Resumo da função" rows="6"></textarea>' +
            '<label for="experiencia-resumo-' + idExp + '">Resumo da função (opcional)</label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">' +
            '<div class="form-group">' +
            '<div class="col-sm-12">' +
            '<select id="experiencia-habilidades-' + idExp + '" class="form-control check-empty select-add" name="habilidades-' + idExp + '" onchange="adicionarTags(this)">' +
            '<option hidden></option>' +
            '<option value="frontend">Frontend</option>' +
            '<option value="photoshop">Photoshop</option>' +
            '</select>' +
            '<label for="experiencia-habilidades-' + idExp + '">Adicionar principais habilidades</label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">' +
            '<ul class="list-tags experiencia-habilidades-' + idExp + '-tags list-inline"></ul>' +
            '</div>' +
            '</div>' +
            '</div>').hide().fadeIn(300);

    setTimeout(function () {
        $('.card-' + idExp).find('select').each(function () {

            if ($(this).hasClass('select-add')) {
                $(this).selectize({
                    create: function (input) {
                        return {value: slug(input), text: input};
                    }
                });
            } else {
                $(this).selectize();
            }
        });
        setTimeout(function () {
            validacao();
            $('.card-inputs').closest('form').find('input')[0].focus();
        }, 100);
    }, 100);
}
function adicionarGraduacao() {

    if (!$('.card-inputs').closest('form').get(0).checkValidity()) {
        $("#" + document.querySelectorAll(":invalid")[1].id).focus();
        alertInfo();
        return false;
    }

    var exp = $('.card-input-titulo');
    var countExp = exp.length;
    var idExp;

    if (countExp > 0) {
        idExp = parseInt(exp.last().attr('id').split('-').pop()) + 1;
    } else {
        idExp = 1;
    }

    $('.card-inputs').append('<div class="col-md-6 col-md-offset-3 col-xs-12 bg-primary-sweet card-' + idExp + '">' +
            '<div class="graduacao-' + idExp + '">' +
            '<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 card-input-titulo" id="grad-titulo-' + idExp + '">' +
            '<h5>Graduação ' + idExp + '</h5>' +
            '<i class="fa fa-times-circle remover_card" onclick="removerCardInputs(this)" aria-hidden="true" style="cursor: pointer;"></i>' +
            '</div>' +
            '<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">' +
            '<div class="form-group">' +
            '<div class="col-md-12">' +
            '<input type="text" class="form-control check-empty" name="nome-instituicao[]" id="nome-instituicao-' + idExp + '" pattern=".{3,}" title="Nome da instituição de ensino" required>' +
            '<label for="nome-instituicao-' + idExp + '">Nome da instituição de ensino</label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 col-md-offset-2 col-sm-4 col-sm-offset-2 col-xs-12">' +
            '<div class="form-group">' +
            '<div class="col-md-12">' +
            '<input type="tel" class="form-control data-mask check-empty" name="data-inicio[]" id="data-inicio-' + idExp + '" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" title="Data de início" required>' +
            '<label for="data-inicio-' + idExp + '">Data de início</label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 col-sm-4 col-xs-12">' +
            '<div class="form-group">' +
            '<div class="col-md-12">' +
            '<input type="tel" class="form-control data-mask check-empty" name="data-fim[]" id="experiencia-data-fim-' + idExp + '" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" title="Data de término" required>' +
            '<label for="experiencia-data-fim-' + idExp + '">Data de término</label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">' +
            '<div class="form-group">' +
            '<div class="col-md-12">' +
            '<input type="text" class="form-control check-empty" name="curso[]" id="curso-' + idExp + '" pattern=".{2,}" title="Curso" required>' +
            '<label for="curso-' + idExp + '">Curso</label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>').hide().fadeIn(300);

    setTimeout(function () {
        validacao();
        $('.card-inputs').closest('form').find('input')[0].focus();
    }, 100);
}
function adicionarPublicacao() {

    if (!$('.card-inputs').closest('form').get(0).checkValidity()) {
        $("#" + document.querySelectorAll(":invalid")[1].id).focus();
        alertInfo();
        return false;
    }

    var exp = $('.card-input-titulo');
    var countExp = exp.length;
    var idExp;

    if (countExp > 0) {
        idExp = parseInt(exp.last().attr('id').split('-').pop()) + 1;
    } else {
        idExp = 1;
    }

    $('.card-inputs').append('<div class="col-md-6 col-md-offset-3 col-xs-12 bg-primary-sweet card-' + idExp + '">' +
            '<div class="publicacao-' + idExp + '">' +
            '<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 card-input-titulo" id="publ-titulo-' + idExp + '">' +
            '<h5>Publicação ' + idExp + '</h5>' +
            '<i class="fa fa-times-circle remover_card" onclick="removerCardInputs(this)" aria-hidden="true" style="cursor: pointer;"></i>' +
            '</div>' +
            '<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">' +
            '<div class="form-group">' +
            '<div class="col-md-12">' +
            '<input type="text" class="form-control check-empty" name="url[]" id="url-' + idExp + '" pattern=".{3,}" title="Adicione o link da publicação" required>' +
            '<label for="url-' + idExp + '">http://</label>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>').hide().fadeIn(300);

    setTimeout(function () {
        validacao();
        $('.card-inputs').closest('form').find('input')[0].focus();
    }, 100);
}

function removerTag(rm) {
    var li = $(rm).parent();
    li.fadeOut(300, function () {
        $(this).remove();
    });
}
function removerCard(rm) {
    var li = $(rm).parent().parent();
    li.fadeOut(300, function () {
        $(this).remove();
    });
}
function removerCardInputs(rm) {

    var exp = $(rm).closest('.bg-primary-sweet');
    exp.fadeOut(300, function () {
        $(this).remove();
    });
}

function slug(str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove acentos
    var from = "ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;";
    var to = "AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------";
    for (var i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove caracteres especiais
            .replace(/\s+/g, '-') // substitui espaço em branco po hifen "-"
            .replace(/-+/g, '-'); // substitui espaço em branco seguid ode hifen por hifen "-"

    return str;
}