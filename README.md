# Café com vagas #
Arquivos de front-end (HTML, CSS, JS) para projeto Café com vagas.

##### Sobre #####
http://aws.vinteum.com/pluguin/cafecomvagas/sobre.html

##### Termos de uso #####
http://aws.vinteum.com/pluguin/cafecomvagas/termos-de-uso.html

##### Política de privacidade #####
http://aws.vinteum.com/pluguin/cafecomvagas/politica-privacidade.html

##### Contato #####
http://aws.vinteum.com/pluguin/cafecomvagas/contato.html

##### FAQ #####
http://aws.vinteum.com/pluguin/cafecomvagas/faq.html

### Candidato ###

##### Página inicial #####
http://aws.vinteum.com/pluguin/cafecomvagas/

##### Buscar vagas #####
http://aws.vinteum.com/pluguin/cafecomvagas/buscar-vaga.html

##### Vagas candidatadas #####
http://aws.vinteum.com/pluguin/cafecomvagas/vagas-candidatada.html

##### Minha conta #####
http://aws.vinteum.com/pluguin/cafecomvagas/minha-conta.html

#### Cadastro do candidato: ####
##### Dados pessoais #####
http://aws.vinteum.com/pluguin/cafecomvagas/usuario/cadastro/dados-pessoais.html

##### Dados pessoais endereço #####
http://aws.vinteum.com/pluguin/cafecomvagas/usuario/cadastro/dados-pessoais-endereco.html

##### Dados pessoais link #####
http://aws.vinteum.com/pluguin/cafecomvagas/usuario/cadastro/dados-pessoais-link.html

##### Top 5 habilidades #####
http://aws.vinteum.com/pluguin/cafecomvagas/usuario/cadastro/habilidades-destaque.html

##### Experiência profissional #####
http://aws.vinteum.com/pluguin/cafecomvagas/usuario/cadastro/experiencia-profissional.html

##### Graduação #####
http://aws.vinteum.com/pluguin/cafecomvagas/usuario/cadastro/graduacao.html

##### Cursos #####
http://aws.vinteum.com/pluguin/cafecomvagas/usuario/cadastro/cursos.html

##### Idiomas #####
http://aws.vinteum.com/pluguin/cafecomvagas/usuario/cadastro/idioma.html

##### Publicações #####
http://aws.vinteum.com/pluguin/cafecomvagas/usuario/cadastro/publicacao.html

---------------------------------------------------------------------------------------------------------------

### Empresa ###

##### Página inicial #####
http://aws.vinteum.com/pluguin/cafecomvagas/empresa.html

#### Cadastro da empresa: ####
##### Dados pessoais #####
http://aws.vinteum.com/pluguin/cafecomvagas/empresa/cadastro/cadastro.html

##### Dados empresa #####
http://aws.vinteum.com/pluguin/cafecomvagas/empresa/cadastro/dados-empresa.html

##### Inicio #####
http://aws.vinteum.com/pluguin/cafecomvagas/empresa/inicio.html

##### Minha conta #####
http://aws.vinteum.com/pluguin/cafecomvagas/empresa/minha-conta.html

##### Dados da empresa #####
http://aws.vinteum.com/pluguin/cafecomvagas/empresa/dados-empresa.html

##### Publicar vagas #####
http://aws.vinteum.com/pluguin/cafecomvagas/empresa/publicar-vaga.html

##### Criar vagas #####
http://aws.vinteum.com/pluguin/cafecomvagas/empresa/vagas/criar-vaga.html

##### Minhas vagas #####
http://aws.vinteum.com/pluguin/cafecomvagas/empresa/vagas/minhas-vagas.html

##### Minhas vagas - Candidatos #####
http://aws.vinteum.com/pluguin/cafecomvagas/empresa/vagas/minhas-vagas-candidatos.html

---------------------------------------------------------------------------------------------------------------

### Email Template ###

##### Confirmar Conta #####
http://aws.vinteum.com/pluguin/cafecomvagas/email/confirmar-conta.html

##### Recuperar senha #####
http://aws.vinteum.com/pluguin/cafecomvagas/email/recuperar-senha.html
